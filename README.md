1) Если у вас нету Ansible, то скачайте его, введя в терминале: **sudo apt update**, а затем **sudo apt install ansible**
2) Скачайте роль: git clone https://gitlab.com/polygon4/promobit.git 
3) Перейдите в директорию проекта, введя в терминале: cd promobit
4) Запустите ansible-роль в терминале: ansible-playbook -i inventory nginx-role.yml
5) Зайдите в браузер и введите в поисковой строке localhost

